create database DBPenerbit;
use DBPenerbit;

create table tblPengarang(
ID int primary key identity(1,1) not null,
KD_Pengarang varchar(7) not null,
Nama varchar(30) not null,
Alamat varchar(80) not null,
Kota varchar(15) not null,
Kelamin varchar(1) not null
)

create table tblGaji(
ID int primary key identity(1,1) not null,
KD_Pengarang varchar(7) not null,
Nama varchar(30) not null,
Gaji decimal(18,4) not null
)

insert into tblPengarang(
KD_Pengarang,
Nama,
Alamat,
Kota,
Kelamin
)values
('P0001','Ashadi','Jl. Beo 25','Yogya','p'),
('P0002','Rian','Jl. Solo 123','Yogya','p'),
('P0003','Suwandi','Jl. Semangka 13','Bandung','p'),
('P0004','Siti','Jl. Durian 15','Solo','W'),
('P0005','Amir','Jl. Gajah 33','Kudus','p'),
('P0006','Suparman','Jl. Harimau 25','Jakarta','p'),
('P0007','Jaja','Jl. Singa 7','Bandung','p'),
('P0008','Saman','Jl. Naga 12','Yogya','p'),
('P0009','Anwar','Jl. Tidar 6 A','Magelang','p'),
('P00010','Fatmawati','Jl. Renjana 4','Bogor','W')

update tblPengarang
set KD_Pengarang= 'P0010'
where ID = 10

select * from tblPengarang

update tblGaji
set Nama = 'Suwadi'
where KD_Pengarang='P0003'


insert into tblGaji(
KD_Pengarang,
Nama,
Gaji
)values
('P0002','Rian',600000),
('P0005','Amir',700000),
('P0004','Siti',500000),
('P0003','Suwadi',1000000),
('P00010','Fatmawati',600000),
('P0008','Saman',750000)

update tblGaji
set KD_Pengarang= 'P0010'
where ID = 5

select * from tblGaji;







--Soal NO1

select count(*) from tblPengarang;












--Soal NO2

select kelamin, count(*) as Jumlah
from tblPengarang
group by Kelamin
having Kelamin='p' OR Kelamin = 'W'









--Soal NO3

select Kota,count(*) as Jumlah
from tblPengarang
group by Kota









--Soal NO 4

select Kota,count(*)
from tblPengarang
group by Kota
having count(*)>1









--Soal No 5
select MAX(KD_Pengarang) as KD_Terbesar, MIN(KD_Pengarang) as KD_Terkecil
from tblPengarang








--soal no 6
select MAX(Gaji) as GajiTertinggi, MIN(Gaji) as GajiTerendah
from tblGaji







--soal no 7
select Gaji
from tblGaji
where Gaji>600000








--soal no 8
select sum(Gaji) as TotalGaji
from tblGaji











--soal no9
Update tblPengarang 
set Kota='Yogya'
where Nama='Rian'

select sum(G.Gaji) as TotalGaji,P.Kota
from tblGaji as G join tblPengarang as P
ON G.KD_Pengarang=P.KD_Pengarang
group by P.Kota
Having P.Kota=P.Kota






--soal no10

Select * from
tblPengarang
where KD_Pengarang between 'P0001' and 'P0006'







--Soal no 11
Select * from 
tblPengarang
where Kota='Solo' or Kota = 'Magelang' or Kota='Yogya'






--Soal no 12

Select * from 
tblPengarang
where Kota !='Yogya'






-- soal no 13
select * from tblPengarang
where Nama Like 'A%'

select * from tblPengarang
where Nama Like '%i'

select * from tblPengarang
where Nama Like '__a%'

select * from tblPengarang
where Nama Not Like '%n'





-- soal no 14
select * from tblPengarang as P full outer join
tblGaji as G on P.KD_Pengarang=G.KD_Pengarang





--soal no 15
select G.Gaji as TotalGaji,P.Kota
from tblGaji as G join tblPengarang as P
ON G.KD_Pengarang=P.KD_Pengarang
where G.Gaji<1000000




--soal no 16
alter table tblPengarang add Kelamin varchar(10);






--soal no 17

alter table tblPengarang add Gelar varchar(12);







--soal no 18

update tblPengarang
set Alamat='Jl. Cendrawasih 65', Kota='Pekan Baru'
where Nama='Rian'





--soal no 19
create view vwPengarang
as
select KD_Pengarang, Nama, Kota 
from tblPengarang;

select * from vwPengarang


--4.    Tampilkan record kota diatas 1 kota dari table tblPengarang.
--5.    Tampilkan Kd_Pengarang yang terbesar dan terkecil dari table tblPengarang.
--6.    Tampilkan gaji tertinggi dan terendah.

--4
select count(Kota),Kota from tblPengarang 
group by Kota
having count(Kota)>1

--5 
select Max(KD_Pengarang) as KDTerbesar, Min(KD_Pengarang) as KDTerkecil 
from tblPengarang 

--6
select Max(Gaji) as GajiTerkecil, Min(Gaji) as GajiTerbesar
from tblGaji
  
