create database DBKaryawan
use DBKaryawan

create table tb_karyawan(
id  bigint primary key identity(1,1) NOT NULL,
nomor_induk varchar(7) NOT NULL,
nama varchar(30) NOT NULL,
alamat text NOT NULL,	
tanggal_lahir date NOT NULL,
tanggal_masuk date NOT NULL
)

insert into tb_karyawan(
nomor_induk,
nama,
alamat,
tanggal_lahir,
tanggal_masuk
)values
('IP06001','Agus','JL. Gajah Mada 115A, Jakarta Pusat','8-1-70', '7-7-06'),
('IP06002','Amin','Jl. Bungur Sari V, NO. 178, Bandung','5/3/77','7/7/6'),
('IP06003','Yusuf','Jln. Bungur sari v No, 178, Bandung','8/9/73','7/8/06'),
('IP06004','Alysaa','Jln. Yosodpuro 15, Surabaya','2/14/83','1/5/07'),
('IP07005','Maulana',	'Jln. Ampera Raya No 1',	'10/10/85',	'2/5/07'),
('IP07006','Afika',	'Jln. Pejaten Barat No 6A',	'3/9/87	', '6/9/07'),
('IP07007','James',	'Jln. Padjadjaran No. 111, bandung',	'5/19/88',	'6/9/06'),
('IP09008','Octavanus',	'Jln. Gajah Mada 101. Semarang',	'10/7/88',	'8/8/08'),
('IP09009','Nugroho',	'Jln. Duren Tiga 196, Jakarta selatan',	'1/20/88',	'11/11/08'),
('IP09010','Raisa',	'Jln. Nangka Jakarta selatan',	'12/29/89',	'2/9/09')
select * from tb_karyawan

create table cuti_karyawan(
id  bigint PRIMARY KEY identity  NOT NULL ,
nomor_induk varchar(7) NOT NULL,
tanggal_mulai date NOT NULL,
lama_cuti int NOT NULL,
keterangan text NOT NULL
)

insert into cuti_karyawan(
nomor_induk,
tanggal_mulai ,
lama_cuti,
keterangan
)
values
('IP06001','2/1/12','3','Acara keluar'),
('IP06001'	,'2/13/12',	'4',	'Anak sakit'),
('IP07007'	,'2/15/12',	'2',	'Nenek sakit'),
('IP06003'	,'2/17/12',	'1',	'Mendaftar sekolah anak'),
('IP07006'	,'2/20/12',	'5',	'Menikah'),
('IP07004'	,'2/27/12',	'1',	'Imunisasi anak')

--1
select top(3) nama,tanggal_masuk from tb_karyawan
order by tanggal_masuk asc

--2

select tb_karyawan.nomor_induk,nama,DATEADD (day,lama_cuti,tanggal_mulai) as jumlah_cuti
,cuti_karyawan.keterangan from tb_karyawan 
left join cuti_karyawan on 
tb_karyawan.nomor_induk=cuti_karyawan.nomor_induk
where DATEADD(day,lama_cuti,tanggal_mulai)<'12/16/12'

--3
select tb_karyawan.nomor_induk,nama,
sum(cuti_karyawan.lama_cuti) from tb_karyawan 
left join cuti_karyawan on 
tb_karyawan.nomor_induk=cuti_karyawan.nomor_induk
where lama_cuti is not null and lama_cuti>1
group by tb_karyawan.nomor_induk, tb_karyawan.nama

--4
select tb_karyawan.nomor_induk,nama,12-SUM(ISNULL(cuti_karyawan.lama_cuti,0))
from tb_karyawan 
left join cuti_karyawan on 
tb_karyawan.nomor_induk=cuti_karyawan.nomor_induk
group by tb_karyawan.nomor_induk, tb_karyawan.nama







