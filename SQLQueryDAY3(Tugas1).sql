create database db_artis

use db_artis



create table artis(
kd_artis character varying(100) primary key,
nm_artis character varying(100),
jk character varying(100),
bayaran int,
award int,
negara character varying(100)
)

insert into artis(

kd_artis ,
nm_artis ,
jk ,
bayaran ,
award,
negara
)
values
('A001','ROBERT DOWNEY JR','PRIA',2000000000,2,'AS'),
('A002','ANGELINA JOLIE','WANITA',700000000,1,'AS'),
('A003','JACKIE CHAN','PRIA',200000000,7,'HK'),
('A004','JOE TASLIM','PRIA',350000000,1,'ID'),
('A005','CHELSEA ISLAN','WANITA',300000000,0,'ID')

SELECT * FROM ARTIS

create table negara(
kd_negara character varying(100) primary key,
nm_negara character varying(100)
)

insert into negara(
kd_negara,
nm_negara
)
values
('AS','AMERIKA SERIKAT'),
('HK','HONGKONG'),
('ID','INDONESIA'),
('IN','INDIA')

SELECT * FROM NEGARA

CREATE TABLE genre(
kd_genre character varying(50) primary key,
nm_genre character varying(50)
)

insert into genre(
kd_genre,
nm_genre
)values
('G001','ACTION'),
('G002','HORROR'),
('G003','COMEDY'),
('G004','DRAMA'),
('G005','THRILLER'),
('G006','FICTION')

SELECT * FROM GENRE

--1 kecil - besar

CREATE TABLE produser(
kd_produser character varying(50) primary key,
nm_produser character varying(50),
internasional character varying(50)
)

insert into produser(
kd_produser,
nm_produser,
internasional
)


values('PD01','MARVEL','YA'),
('PD02','HONGKONG','YA'),
('PD03','RAPI FILM','YA'),
('PD04','PARKIT','YA'),
('PD05','PARAMOUNT PICTURE','YA')

UPDATE produser
set kd_produser='PD05'
where nm_produser='PARAMOUNT PICTURE'

SELECT * FROM produser


create table film(
kd_film character varying(55) primary key,
nm_film character varying(55),
genre character varying(55),
artis character varying(55),
produser character varying(55),
pendapatan int,
nominasi int
)


insert into film(
kd_film ,
nm_film ,
genre ,
artis ,
produser ,
pendapatan,
nominasi
)values
('F001','IRON MAN','G001','A001','PD01',2000000000,3),
('F002','IRON MAN 2','G001','A001','PD01',1800000000,2),
('F003','IRON MAN 3','G001','A001','PD01',1200000000,0),
('F004','AVENGER CIVIL WAR','G001','A001','PD01',2000000000,1),
('F005','SPIDERMAN HOME COMING','G001','A001','PD01',1300000000,0),
('F006','THE RAID','G001','A004','PD03',800000000,5),
('F007','FAST & FURIOUS','G001','A004','PD05',830000000,2),
('F008','HABIBIE DAN AINUN','G004','A005','PD03',670000000,4),
('F009','POLICE STORY','G001','A003','PD02',700000000,3),
('F010','POLICE STORY 2','G001','A003','PD02',710000000,1),
('F011','POLICE STORY 3','G001','A003','PD02',615000000,0),
('F012','RUSH HOUR','G003','A003','PD05',695000000,2),
('F013','KUNGFU PANDA','G003','A003','PD05',923000000,5)

alter table film alter column pendapatan decimal(18,2)
alter table film alter column bayaran decimal(18,2)

--NO1
select nm_film,
(select min(pendapatan) 
from film
)
from film
where pendapatan=(select min(pendapatan) 
from film
)


--NO2

select nm_film,
(select MAX(nominasi) 
from film
)
from film
where nominasi=(select MAX(nominasi) 
from film
)

--no3

select nm_film,
(select min(nominasi) 
from film
)
from film
where nominasi=(select min(nominasi) 
from film
)

--no4 
select nm_film,nominasi from film 
order by nominasi asc

--no5

select nm_film,
(select max(pendapatan) 
from film
)
from film
where pendapatan=(select max(pendapatan) 
from film
)

--6
select nm_film
from film
where nm_film like 'P%'
--7
select nm_film
from film
where nm_film like '%y'
--8
select nm_film
from film
where nm_film like '%d%'

--9

select nm_film,
(select max(pendapatan) 
from film
)
from film
where pendapatan=(select max(pendapatan) 
from film
) and nm_film like '%o%'

--10
select nm_film,
(select min(pendapatan) 
from film
)
from film
where pendapatan=(select min(pendapatan) 
from film
) and nm_film like '%o%'

--11

select film.nm_film,artis.nm_artis from film left join artis on film.artis=artis.kd_artis

--12
select nm_negara,film.nm_film,artis.nm_artis from film left join artis on film.artis=artis.kd_artis 
left join negara on artis.negara=negara.kd_negara
where negara.nm_negara='HONGKONG'

--13

select nm_negara,film.nm_film,artis.nm_artis from film left join artis on film.artis=artis.kd_artis 
left join negara on artis.negara=negara.kd_negara
where negara.nm_negara not like '%o%'

--14

select nm_artis from artis left join film on artis.kd_artis=film.artis
where kd_film is null

--15

select nm_artis from artis left join film on artis.kd_artis=film.artis
left join genre on genre.kd_genre=film.genre
where genre.nm_genre='DRAMA'

--16

select nm_artis from artis left join film on artis.kd_artis=film.artis
left join genre on genre.kd_genre=film.genre
where genre.nm_genre='ACTION'

--17

select negara.nm_negara, count(film.kd_film)  from artis right join film on artis.kd_artis=film.artis
right join negara on negara.kd_negara=artis.negara
group by negara.nm_negara



--18
select film.nm_film from artis right join film on artis.kd_artis=film.artis
left join negara on negara.kd_negara=artis.negara left join produser on produser.kd_produser=film.produser
group by nm_film, internasional
having produser.internasional='YA'

--19
select count(film.nm_film),produser.nm_produser from artis right join film on artis.kd_artis=film.artis
left join negara on negara.kd_negara=artis.negara left join produser on produser.kd_produser=film.produser
group by produser.nm_produser


--20
select sum(film.pendapatan),produser.nm_produser from artis right join film on artis.kd_artis=film.artis
left join negara on negara.kd_negara=artis.negara left join produser on produser.kd_produser=film.produser
group by produser.nm_produser
having produser.nm_produser='MARVEL'
