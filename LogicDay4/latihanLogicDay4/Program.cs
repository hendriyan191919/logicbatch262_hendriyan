﻿using System;

namespace latihanLogicDay4
{
    class Program
    {
        static void Main(string[] args)
        {
            bool yn = true;

            while (yn == true)
            {
                Console.Write("Masukan No soal 1-5 : ");
                int soal = int.Parse(Console.ReadLine());


                switch (soal)
                {
                    case 1:
                        soalKe1();
                        break;
                    case 2:
                        soalKe2();
                        break;
                    case 3:
                        soalKe3();
                        break;
                    case 4:
                        soalKe4();
                        break;
                    case 5:
                        soalKe5();
                        break;

                }

                Console.Write("Ingin ulangi soal? ");
                string yesno = Console.ReadLine().ToLower();


                if (yesno == "n")
                {
                    yn = false;

                }
                else if (yesno == "y")
                {
                    yn = true;
                }
                else
                {

                    break;

                }
                Console.Clear();
            }


            void soalKe1()
            {
                Console.Write("Masukan Jam : ");
                string jm = Console.ReadLine();
                string hari = jm.Substring(8);
                string jam = jm.Substring(0, 2);
                string jam2 = jm.Substring(2, 6);
                int jam3 = int.Parse(jam);
                Console.WriteLine("Input Jam :"+jm);
                //07:05:45PM
                if (jam3 <= 24)
                {

                    if (hari == "PM" && jam3 < 12)
                    {
                        jam3 = jam3 + 12;
                        jam2 = jam3 + jam2;
                        Console.WriteLine("Jam : " + jam2);
                    }
                    else if (hari == "PM" && jam3 > 12)
                    {
                        jam2 = jam3 + jam2;
                        Console.WriteLine("Jam : " + jam2);
                    }
                    else if (hari == "AM" && jam3 >= 12)
                    {
                        jam3 = jam3 - 12;
                        jam2 = jam3 + jam2;
                        Console.WriteLine("Jam : " + jam2);
                    }
                    else if (hari == "AM" && jam3 < 12)
                    {

                        jam2 = jam3 + jam2;
                        Console.WriteLine("Jam : " + jam2);
                    }
                }
                else {
                    Console.Clear();
                    Console.WriteLine("Jam Melebihi 24:00");
                    Console.ReadKey();
                }

            }
            void soalKe2()
            {

                



            }



            void soalKe3()
            {
               
                int jamMasuk;
                int jamKeluar;
                int menitMasuk;
                int menitKeluar;
                int menitTotal;
                int jamTotal;
                int bayarParkir;
                int jmMasuk, mntMasuk,jmKeluar, mntKeluar;
                Console.WriteLine("Masukan Jam Masuk: ");
                Console.WriteLine("Jam: ");
                jmMasuk = int.Parse(Console.ReadLine());
                Console.WriteLine("Menit: ");
                mntMasuk = int.Parse(Console.ReadLine());

                Console.WriteLine("Masukan Jam Keluar: ");
                Console.WriteLine("Jam: ");
                jmKeluar = int.Parse(Console.ReadLine());
                Console.WriteLine("Menit: ");
                mntKeluar = int.Parse(Console.ReadLine());

                TimeSpan jam1 = new TimeSpan(jmMasuk,mntMasuk,0);
                TimeSpan jam2 = new TimeSpan(jmKeluar,mntKeluar,0);
            

                Console.WriteLine("Jam Masuk = " + jam1.Hours + ":" + jam1.Minutes);
                Console.WriteLine("Jam Keluar = " + jam2.Hours + ":" + jam2.Minutes);
                menitMasuk = jam1.Minutes;
                menitKeluar = jam2.Minutes;
                jamMasuk = jam1.Hours;
                jamKeluar = jam2.Hours;
                menitMasuk = jam1.Minutes;
                menitKeluar = jam2.Minutes;
                menitMasuk = jam1.Minutes;
                jamTotal = jamKeluar - jamMasuk;
                menitTotal = menitKeluar - menitMasuk;

                Console.WriteLine("Total Jam Parkir: " + jamTotal);
                bayarParkir = jamTotal * 3000;
                menitTotal = menitKeluar - menitMasuk;
                
                if (menitTotal<=30)
                {
                    bayarParkir = bayarParkir + 3000;

                }
                Console.WriteLine("Total Bayar: " + bayarParkir);



            }


            void soalKe4()
            {

                //Console.Write("Masukan Tanggal : ");
                //string tgl = Console.ReadLine();
                DateTime date1 = new DateTime(2019, 8, 20, 0, 0, 0, 0);
                DateTime date2 = new DateTime(2019, 10, 17, 0, 0, 0, 0);
                int hariPengembalian = 3;
                int denda;
                int hari;
                Console.WriteLine("Tanggal Pinjam : " + date1.Year + "/" + date1.Month + "/" + date1.Day);
                Console.WriteLine("Tanggal Kembali : " + date2.Year + "/" + date2.Month + "/" + date2.Day);
                TimeSpan keterlambatan = date2 - date1;
                Console.WriteLine("Keterlambatan: " + (keterlambatan.Days - hariPengembalian));
                hari = keterlambatan.Days - hariPengembalian;
                denda = hari * 500;
                Console.WriteLine("Denda: " + denda);


            }


            void soalKe5()
            {

                DateTime tanggalMasuk = new DateTime(2021, 02, 26);
                DateTime tanggalOutput = tanggalMasuk;
                int libur = 11;
                int waktuBelajar = 10;
                int temp = 0;
                // int loop = 2;
                bool isTrue = true;
                int i = 0;

                while (isTrue)
                {
                    i++;
                    tanggalOutput = tanggalMasuk.AddDays(i);
                    if (tanggalOutput.DayOfWeek == DayOfWeek.Saturday || tanggalOutput.DayOfWeek == DayOfWeek.Sunday || i == libur)
                    {
                        continue;
                    }
                    else
                    {
                        temp++;
                    }

                    if (temp == waktuBelajar)
                    {
                        isTrue = false;
                    }
                }
                Console.WriteLine(tanggalOutput.ToString("MMMM dd, yyyy"));


            }







        }
    }
}
