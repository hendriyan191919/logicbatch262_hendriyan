create database DB_HR
use DB_HR

create table tb_divisi(
id bigint primary key identity (1,1) not null,
kd_divisi character varying(50) not null,
nama_divisi character varying(50) not null
)

insert into tb_divisi(
kd_divisi,
nama_divisi
)values('GD','Gudang'),
('HRD','HRD'),
('KU','Keuangan'),
('UM','Umum')

select * from tb_divisi

create table tb_jabatan(
id bigint primary key identity (1,1) not null,
kd_jabatan character varying (50) not null,
nama_jabatan character varying (50) not null,
gaji_pokok numeric,
tunjangan_jabatan numeric,
)

insert into tb_jabatan(
kd_jabatan,
nama_jabatan,
gaji_pokok,
tunjangan_jabatan
) values
('MGR','Manager',5500000,1500000),
('OB','Office Boy',1900000,200000),
('ST','Staff',3000000,750000),
('WMGR','Wakil Manager',4000000,1200000)

select * from tb_jabatan

create table tb_karyawan(
id bigint primary key identity (1,1) not null,
nip character varying(50) not null,
nama_depan character varying(50) not null,
nama_belakang character varying(50) not null,
jenis_kelamin character varying(50) not null,
agama character varying(50) not null,
tempat_lahir character varying(50) not null,
tgl_lahir date,
alamat character varying(100) not null,
pendidikan_terakhir character varying(50) not null,
tgl_masuk date
)

insert into tb_karyawan(
nip,
nama_depan,
nama_belakang,
jenis_kelamin,
agama,
tempat_lahir,
tgl_lahir,
alamat,
pendidikan_terakhir,
tgl_masuk
)
values
('001','Hamidi','Samsudin','Pria','Islam','Sukabumi','1997-04-21','Jl. Sudirman No.12','S1 Teknik Mesin','2015-12-07'),
('003','Paul','Christian','Pria','Kristen','Ambon','1980-05-27','Jl. Veteran No.4','S1 Pendidikan Geografi','2014-01-12'),
('002','Ghandi','Wamida','Wanita','Islam','Palu','1992-01-12','Jl. Rambutan No.22','SMA Negeri 02 Palu','2014-12-01')

select * from tb_karyawan

create table tb_pekerjaan(
id bigint primary key identity (1,1) not null,
nip character varying (50) not null,
kode_jabatan character varying (50) not null,
kode_divisi character varying (50) not null,
tunjangan_kinerja numeric,
kota_penempatan character varying (50)
)

insert into tb_pekerjaan(
nip,
kode_jabatan,
kode_divisi,
tunjangan_kinerja,
kota_penempatan
)values
('001','ST','KU',750000,'Cianjur'),
('002','OB','UM',350000,'Sukabumi'),
('003','MGR','HRD',1500000,'Sukabumi')

select * from tb_pekerjaan;


--1

select concat (a.nama_depan+' ',a.nama_belakang) 
as Nama_Lengkap,c.nama_jabatan ,(c.gaji_pokok+c.tunjangan_jabatan) as gaji_tunjangan  from tb_karyawan as a 
left join tb_pekerjaan b on a.nip=b.nip
left join tb_jabatan as c on b.kode_jabatan=c.kd_jabatan 
where (c.gaji_pokok+c.tunjangan_jabatan)<5000000


--2


select concat (a.nama_depan+' ',a.nama_belakang) 
as Nama_Lengkap,c.nama_jabatan, e.nama_divisi ,
(c.gaji_pokok+c.tunjangan_jabatan+d.tunjangan_kinerja) as total_gaji,
(c.gaji_pokok+c.tunjangan_jabatan+d.tunjangan_kinerja)*0.05 as pajak,
(c.gaji_pokok+c.tunjangan_jabatan+d.tunjangan_kinerja)-((c.gaji_pokok+c.tunjangan_jabatan+d.tunjangan_kinerja)*0.05) as gaji_bersih
  from tb_karyawan as a 
right join tb_pekerjaan b on a.nip=b.nip
left join tb_jabatan as c on 
b.kode_jabatan=c.kd_jabatan,
tb_pekerjaan as d right join tb_divisi as e on d.kode_divisi=e.kd_divisi
where a.jenis_kelamin = 'Pria' and a.tempat_lahir='Sukabumi' 

--3

select concat(a.nama_depan,a.nama_belakang) as nama_lengkap, c.nama_jabatan, d.nama_divisi, 
(c.gaji_pokok+c.tunjangan_jabatan+(b.tunjangan_kinerja*7))*0.25 
from tb_karyawan as a left join tb_pekerjaan as b on a.nip=b.nip  
right join tb_jabatan as c on c.kd_jabatan=b.kode_jabatan
join tb_divisi as d on d.kd_divisi=b.kode_divisi 

--4
select concat(nama_belakang,' '+nama_belakang) as nama_lengkap,
tb_jabatan.nama_jabatan,
tb_divisi.nama_divisi,
(gaji_pokok + tunjangan_jabatan +tunjangan_kinerja) as total_gaji,
(gaji_pokok + tunjangan_jabatan +tunjangan_kinerja)*0.05 as infak
  from tb_karyawan
left join tb_pekerjaan on tb_karyawan.nip=tb_pekerjaan.nip
left join tb_jabatan on tb_jabatan.kd_jabatan=tb_pekerjaan.kode_jabatan
left join.tb_divisi on tb_pekerjaan.kode_divisi=tb_divisi.kd_divisi



--5

select concat(nama_depan+' ',nama_belakang) as nama_lengkap, 
tb_jabatan.nama_jabatan,
tb_karyawan.pendidikan_terakhir,
2000000 as tunjangan_pendidikan,
(gaji_pokok + tunjangan_jabatan +tunjangan_kinerja+2000000) as total_gaji
from tb_karyawan
left join tb_pekerjaan on tb_karyawan.nip=tb_pekerjaan.nip
join tb_jabatan on tb_jabatan.kd_jabatan = tb_pekerjaan.kode_jabatan
where tb_karyawan.pendidikan_terakhir like 's1%'

--6

--7
create unique index idx_nip 
on tb_karyawan (nip)

--8

create index idx2_nip 
on tb_karyawan (nip)

--9
select UPPER(concat(nama_depan+' ',nama_belakang)) as NamaLengkap from tb_karyawan
where nama_belakang like 'W%';



