﻿using System;

namespace logicDay2
{
    class Program
    {
        static void Main(string[] args)
        {
            bool yn = true;

            while (yn==true) {
                Console.Write("Masukan No soal (1,2,3,4,5,6,7,8,9) : ");
                int soal = int.Parse(Console.ReadLine());
                

                switch (soal)
            {
                case 1:
                        soalKe1();
                    break;
                case 2:
                        soalKe2();
                    break;
                case 3:
                        soalKe3();
                    break;
                case 4:
                        soalKe4();
                    break;
                case 5:
                        soalKe5();
                        break;
                case 6:
                        soalKe6();
                        break;
                case 7:
                        soalKe7();
                        break;
                case 8:
                        soalKe8();
                        break;
                case 9:
                        soalKe9();
                        break;
                
                }
                
                Console.Write("Ingin ulangi soal? ");
                string yesno = Console.ReadLine().ToLower();


                if (yesno == "n")
                {
                    yn = false;

                } else if (yesno=="y") {
                    yn = true;
                }
                else {

                    break;

                }
                Console.Clear();
            }


            void soalKe1()
            {
                Console.Write("Input Tinggi : ");
                int tinggi = int.Parse(Console.ReadLine());

                for (int a = 1; a <= tinggi; a++)
                {
                    for (int b = 1; b <= a; b++)
                    {
                        Console.Write("  *  ");
                    }
                    Console.WriteLine();
                }

            }
            void soalKe2()
            {
                Console.Write("Input Tinggi : ");
                int tinggi = int.Parse(Console.ReadLine());
                for (int a = 1; a <= tinggi; a++)
                {
                    for (int b = tinggi; b >=a ; b--)
                    {
                        Console.Write("     ");
                    }
                    for (int b = 1; b <= a; b++)
                    {
                        Console.Write("  *  ");
                    }
                    Console.WriteLine();
                }
                

            }

            void soalKe3()
            {
                Console.Write("Masukan Angka (fibonanci 2 ) : ");
                int angka = int.Parse(Console.ReadLine());
                int x = 1;
                int y = 1;
                int z = 1;
                for (int a = 1; a < angka; a++)
                {
                    if (a == 1)
                    {
                        Console.Write(x + "," + y);
                    }
                    else { 
                    z = x + y;
                    Console.Write(","+z);
                    y = x;
                    x = z;
                    
                    }

                }
                Console.WriteLine();
            }


            void soalKe4()
            {
                Console.Write("Masukan Angka (fibonanci 3 ) : ");
                int angka = int.Parse(Console.ReadLine());
                int w = 1;
                int x = 1;
                int y = 1;
                int z = 1;
                for (int a = 0; a < angka; a++)
                {
                    if (a == 1)
                    {
                        Console.Write(w + ","+ x + "," + y);
                    }
                    else
                    {
                        z = w + x + y;
                        Console.Write("," + z);
                        x = y;
                        y = w;
                        w = z;
                        
                        

                    }

                }
                Console.WriteLine();

            }


            void soalKe5()
            {
                Console.WriteLine("Masukan Nilai (bilangan prima)");
                int bil = int.Parse(Console.ReadLine());
               
                for (int x = 0; x < bil; x++) {
                    if (x % 2 == 1 && x!=1 && x%3!=0 && x%5!=0 && x%11!=0||  x==2||x==3||x==5 || x==11) {
                        Console.Write(x+",");    
                    }
                   
                }
                Console.WriteLine();
            }



            void soalKe6()
            {
                Console.WriteLine("Soal Pohon Faktor: ");
                int angka = int.Parse(Console.ReadLine());

                for (int i = 1; i < angka; i++)
                {

                    if (angka % i == 0 && i > 1)
                    {

                        Console.WriteLine(angka + "/" + angka / i + "=" + i);
                        angka = angka / i;
                        Console.WriteLine(i+"/"+i+ "= "+angka / angka);
                    }
                    
                }


            }

            void soalKe7()
            {
                Console.WriteLine("Masukan Angka (Sorting) : ");
                string angka = Console.ReadLine();
                string[] arrAngka = angka.Split(",");
                Console.WriteLine("jumlah angka: "+arrAngka.Length);
                
                for (int a = 0; a < arrAngka.Length; a++) {
                    for (int b = 0; b < arrAngka.Length; b++) {
                        int x = int.Parse(arrAngka[a]);
                        int y = int.Parse(arrAngka[b]);
                        if (x>y)
                        {
                            Console.WriteLine(x);
                        }
                    }


                    }
                
                


            }

            void soalKe8() {
                Console.WriteLine("Masukan Angka N : ");
                int N = int.Parse(Console.ReadLine());
                int i = 1;
                int j = 1;
                Console.Write(N+",");
                while (i<N)
                {
                    j = j * 3;
                    if (i % 2 == 0 && j != 1)
                    {

                        Console.Write("*,");
                    }
                    else {
                        Console.Write(j + ",");
                    }
                    
                    i++;
                }

                Console.WriteLine();
            }

            void soalKe9()
            {
                Console.WriteLine("Masukan Angka N : ");
                int N = int.Parse(Console.ReadLine());
                int i = 1;
                int j = 0;
                Console.Write(N + ",");
                while (i < N-1)
                {
                    j = j+5;
                    if (i % 2 == 1 && j != 1)
                    {

                        Console.Write((j)*-1+",");
                    }
                    else
                    {
                        Console.Write(j + ",");
                    }

                    i++;
                }

                Console.WriteLine();
            }

            

        }

        

    }
}
