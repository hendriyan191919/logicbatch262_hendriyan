create database db_universitas
use db_universitas

create table TB_JURUSANKULIAH(
ID INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
KODE_JURUSAN VARCHAR(12),
DESKRIPSI VARCHAR(50)
)


INSERT INTO TB_JURUSANKULIAH(
KODE_JURUSAN,
DESKRIPSI
)VALUES
('KA001',	'Teknik Informatika'),
('KA002',	'Management Bisnis'),
('KA003',	'Ilmu Komunikasi'),
('KA004',	'Sastra Inggris'),
('KA005',	'Ilmu Pengetahuan Alam Dan Matematika'),
('KA006',	'Kedokteran')

create table TB_DOSEN(
ID INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
KODE_DOSEN VARCHAR(14),
NAMA_DOSEN VARCHAR(25),
KODE_MATAKULIAH VARCHAR(12),
KODE_FAKULTAS VARCHAR(13)
)

INSERT INTO TB_DOSEN(
KODE_DOSEN,
NAMA_DOSEN,
KODE_MATAKULIAH,
KODE_FAKULTAS
)VALUES
('GK001',	'Ahmad Prasetyo',	'KPK001',	'TK002'),
('GK002',	'Hadi Fuladi',	'KPK002',	'TK001'),
('GK003',	'Johan Goerge',	'KPK003',	'TK002'),
('GK004',	'Bima Darmawan'	,'KPK004',	'TK002'),
('GK005',	'Gatot Wahyudi',	'KPK005',	'TK001')

create table TB_MATAKULIAH(
ID INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
KODE_MATA_KULIAH VARCHAR(12),
NAMA_MATAKULIAH VARCHAR(20),
KEAKTIFAN_STATUS VARCHAR(15)
)
ALTER TABLE TB_MATAKULIAH ALTER COLUMN NAMA_MATAKULIAH VARCHAR(30)

INSERT INTO TB_MATAKULIAH(
KODE_MATA_KULIAH,
NAMA_MATAKULIAH,
KEAKTIFAN_STATUS
)VALUES
('KPK001',	'Algoritma Dasar','Aktif'),
('KPK002',	'Basis Data',	'Aktif'),
('KPK003',	'Kalkulus',	'Aktif'),
('KPK004',	'Pengantar', 'Bisnis	Aktif'),
('KPK005',	'Matematika Ekonomi & Bisnis',	'Non Aktif')

create table TB_MAHASISWA(
ID INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
KodeMahasiswa VARCHAR(30),
NamaMahasiswa VARCHAR(30),
AlamatMahasiswa VARCHAR(50),
KodeJurusan VARCHAR(30),
KodeMataKuliah VARCHAR(30)
)
INSERT INTO TB_MAHASISWA
(
KodeMahasiswa,
NamaMahasiswa,
AlamatMahasiswa,
KodeJurusan,
KodeMataKuliah 
)
VALUES
('MK001',	'Fullan',	'Jl. Hati besar no 63 RT.13',	'KA001',	'KPK001'),
('MK002',	'Fullana Binharjo',	'Jl. Biak kebagusan No. 34', 'KA002',	'KPK002'),
('MK003',	'Sardine Himura',	'Jl. Kebajian no 84',	'KA001',	'KPK003'),
('MK004',	'Isani isabul'	,'Jl. Merak merpati No.78',	'KA001',	'KPK001'),
('MK005',	'Charlie birawa'	,'Jl. Air terjun semidi No.56',	'KA003',	'KPK002')

create table TB_NILAI_MAHASISWA(
ID INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
KodeNilai VARCHAR(12),
KodeMahasiswa VARCHAR(12),
KodeOrganisasi VARCHAR(9),
Nilai INT
)

INSERT INTO TB_NILAI_MAHASISWA(
KodeNilai,
KodeMahasiswa,
KodeOrganisasi,
Nilai
)
VALUES
('SK001',	'MK004'	,'KK001',	'90'),
('SK002',	'MK001'	,'KK001',	'80'),
('SK003',	'MK002'	,'KK003',	'85'),
('SK004',	'MK004'	,'KK002',	'95'),
('SK005',	'MK005',	'KK005',	'70')

create table TB_FAKULTAS(
ID INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
KodeFakultas VARCHAR (13),
Penjelasan VARCHAR (30)

)

INSERT INTO TB_FAKULTAS(
KodeFakultas,
Penjelasan
)VALUES
('TK001',	'Teknik Informatika'),
('TK002',	'Matematika'),
('TK003',	'Sistem Infromatika')


CREATE TABLE TB_ORGANISASI(
ID INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
KodeOrganisasi VARCHAR (12),
NamaOrganisasi VARCHAR(50),
StatusOrganisasi VARCHAR(13)
)



INSERT INTO TB_ORGANISASI(
KodeOrganisasi,
NamaOrganisasi,
StatusOrganisasi 
)
VALUES
('KK001',	'Unit Kegiatan Mahasiswa (UKM)', 	'Aktif'),
('KK002',	'Badan Eksekutif Mahasiswa Fakultas (BEMF)',	'Aktif'),
('KK003',	'Dewan Perwakilan Mahasiswa Universitas (DPMU)', 	'Aktif'),
('KK004',	'Badan Eksekutif Mahasiswa Universitas (BEMU)',	'Non Aktif'),
('KK005',	'Himpunan Mahasiswa Jurusan',	'Non Aktif'),
('KK006',	'Himpunan Kompetisi Jurusan', 	'Aktif')


--2
ALTER TABLE TB_DOSEN ALTER COLUMN NAMA_DOSEN VARCHAR(200)

--3
SELECT KodeMahasiswa, NamaMahasiswa, NAMA_MATAKULIAH, DESKRIPSI  FROM TB_MAHASISWA 
LEFT JOIN TB_MATAKULIAH ON 
TB_MAHASISWA.KodeMataKuliah=TB_MATAKULIAH.KODE_MATA_KULIAH
LEFT JOIN TB_JURUSANKULIAH ON
TB_JURUSANKULIAH.KODE_JURUSAN=TB_MAHASISWA.KodeJurusan
WHERE KodeMahasiswa='mk001'

--4
select NAMA_MATAKULIAH,KEAKTIFAN_STATUS,KodeMahasiswa,NamaMahasiswa,
AlamatMahasiswa,KodeJurusan,KODE_MATA_KULIAH 
from TB_MATAKULIAH 
full outer join TB_MAHASISWA on TB_MATAKULIAH.KODE_MATA_KULIAH=TB_MAHASISWA.KodeMataKuliah
where KEAKTIFAN_STATUS='Non Aktif'

--5
select 
TB_MAHASISWA.KodeMahasiswa,
NamaMahasiswa,
AlamatMahasiswa, 
StatusOrganisasi,
Nilai 
from TB_MAHASISWA 
left join TB_NILAI_MAHASISWA on 
TB_MAHASISWA.KodeMahasiswa=TB_NILAI_MAHASISWA.KodeMahasiswa 
left join TB_ORGANISASI 
on TB_ORGANISASI.KodeOrganisasi=TB_NILAI_MAHASISWA.KodeOrganisasi
where StatusOrganisasi='Aktif' and Nilai>=79

--6
select KODE_MATA_KULIAH,NAMA_MATAKULIAH,KEAKTIFAN_STATUS from TB_MATAKULIAH
where NAMA_MATAKULIAH like '%n%'

--7

select count(NamaMahasiswa) as jumlah_organisasi,NamaMahasiswa from TB_MAHASISWA 
left join TB_NILAI_MAHASISWA on TB_MAHASISWA.KodeMahasiswa=TB_NILAI_MAHASISWA.KodeMahasiswa
left join TB_ORGANISASI on TB_ORGANISASI.KodeOrganisasi=TB_NILAI_MAHASISWA.KodeOrganisasi
group by NamaMahasiswa
having count(NamaMahasiswa)>1

--8
select KodeMahasiswa, NamaMahasiswa, NAMA_MATAKULIAH, TB_JURUSANKULIAH.DESKRIPSI, 
NAMA_DOSEN, TB_MATAKULIAH.KEAKTIFAN_STATUS, TB_FAKULTAS.Penjelasan as fakultas from TB_MAHASISWA 
left join TB_DOSEN on TB_DOSEN.KODE_MATAKULIAH = TB_MAHASISWA.KodeMataKuliah 
left join TB_MATAKULIAH on TB_MAHASISWA.KodeMataKuliah = TB_MATAKULIAH.KODE_MATA_KULIAH 
left join TB_JURUSANKULIAH on TB_JURUSANKULIAH.KODE_JURUSAN=TB_MAHASISWA.KodeJurusan
left join TB_FAKULTAS on TB_FAKULTAS.KodeFakultas=TB_DOSEN.KODE_FAKULTAS
where KodeMahasiswa='MK001';

--9
create view vwMahasiswa
as select KodeMahasiswa, NamaMahasiswa, NAMA_MATAKULIAH, TB_JURUSANKULIAH.DESKRIPSI, 
NAMA_DOSEN, TB_MATAKULIAH.KEAKTIFAN_STATUS, TB_FAKULTAS.Penjelasan as fakultas from TB_MAHASISWA 
left join TB_DOSEN on TB_DOSEN.KODE_MATAKULIAH = TB_MAHASISWA.KodeMataKuliah 
left join TB_MATAKULIAH on TB_MAHASISWA.KodeMataKuliah = TB_MATAKULIAH.KODE_MATA_KULIAH 
left join TB_JURUSANKULIAH on TB_JURUSANKULIAH.KODE_JURUSAN=TB_MAHASISWA.KodeJurusan
left join TB_FAKULTAS on TB_FAKULTAS.KodeFakultas=TB_DOSEN.KODE_FAKULTAS

--10

select * from vwMahasiswa
where KodeMahasiswa='MK001';


/*2. Ubah panjang karakter nama dosen menjadi 200
3. tampilkan kode mahasiswa, nama mahasiswa, nama MataKuliah,
 deskripsi Jurusan Kuliah dengan kode mahasiswa  mk001
4. tampilkan nama Mata Kuliah, status Mata Kuliah, 
kode Mahasiswa, nama Mahasiswa, alamat Mahasiswa, 
kode Jurusan, kode Mata Kuliah dengan status Mata Kuliah tidak aktif*/

alter table tb_dosen alter column nama_dosen varchar(200)

select KodeMahasiswa, NamaMahasiswa, NAMA_MATAKULIAH, DESKRIPSI from TB_MAHASISWA
left join TB_MATAKULIAH on TB_MATAKULIAH.KODE_MATA_KULIAH=TB_MAHASISWA.KodeMataKuliah
left join TB_JURUSANKULIAH on TB_JURUSANKULIAH.KODE_JURUSAN=TB_MAHASISWA.KodeJurusan

select NAMA_MATAKULIAH, TB_MATAKULIAH.KODE_MATA_KULIAH, KodeMahasiswa,NamaMahasiswa,
AlamatMahasiswa, KODE_JURUSAN, KODE_MATA_KULIAH, TB_MATAKULIAH.KEAKTIFAN_STATUS
from TB_MAHASISWA
right join TB_MATAKULIAH on TB_MATAKULIAH.KODE_MATA_KULIAH=TB_MAHASISWA.KodeMataKuliah
left join TB_JURUSANKULIAH on TB_JURUSANKULIAH.KODE_JURUSAN=TB_MAHASISWA.KodeJurusan
where KEAKTIFAN_STATUS='Non Aktif'


