create database db_mhsbaru
use db_mhsbaru

create table tblMahasiswa(
nim int primary key identity(101,1),
nama varchar(50),
jenis_kelamin varchar(1),
alamat varchar(50)
)

create table tblAmbilMk(
nim int,
kode_mk varchar (10) 
)



create table matakuliah(
kode_mk varchar(10) primary key,
nama_mk varchar(50) ,
sks int,
semester int,

)

insert into tblMahasiswa(
nama,
jenis_kelamin,
alamat
)values
('Arif','L','Jl. Kenangan'),
('Budi','L','Jl. Jombang'),
('Wati','P','Jl. Surabaya'),
('Ika','P','Jl. Jombang'),
('Tono','L','Jl. Jakarta'),
('Iwan','L','Jl. Bandung'),
('Sari','P','Jl. Malang')
select * from tblMahasiswa

insert into tblAmbilMk(
nim,
kode_mk
)values
('101','PTI447'),
('103','TIK333'),
('104','PTI333'),
('104','PTI777'),
('111','PTI123'),
('123','PTI999')
select * from tblAmbilMk

insert into matakuliah(
kode_mk,
nama_mk,
sks,
semester
)values
('PTI447','Praktikum Basis Data ','1','3'),
('PTI342','Praktikum Basis Data','1','3'),
('PTI333','Basis Data Terdistribusi','3','5'),
('TIK123','Jaringan Komputer','2','5'),
('TIK333','Sistem Operasi','3','5'),
('PTI123','Grafika Multimedia','3','5'),
('PTI777','Sistem Informasi','2','3')
select * from matakuliah
--1
--1
select a.nama,c.nama_mk from tblMahasiswa as a join tblAmbilMk as b on a.nim=b.nim join
matakuliah c on b.kode_mk=c.kode_mk
--2
select * from tblMahasiswa as a full outer join tblAmbilMk as b on a.nim=b.nim 
where b.kode_mk is NULL AND b.nim is Null
--3
select count(*),jenis_kelamin from tblMahasiswa as a full outer join tblAmbilMk as b on a.nim=b.nim 
where b.kode_mk is NULL AND b.nim is Null
group by a.jenis_kelamin
--4
select a.nim,a.nama,c.nama_mk,c.kode_mk from tblMahasiswa as a join tblAmbilMk as b on a.nim=b.nim join
matakuliah c on b.kode_mk=c.kode_mk
--5
select b.nim,count(c.sks) from tblMahasiswa as a join tblAmbilMk as b on a.nim=b.nim join
matakuliah c on b.kode_mk=c.kode_mk
group by b.nim
--6
select a.nim,a.nama,c.nama_mk,c.kode_mk from tblMahasiswa as a full outer join tblAmbilMk as b on a.nim=b.nim full outer join
matakuliah c on b.kode_mk=c.kode_mk
where a.nim is NULL